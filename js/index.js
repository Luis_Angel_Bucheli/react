function getPokemon(pkId) {
  let fetchData = fetch(`https://pokeapi.co/api/v2/pokemon/${pkId}`)
    .then((response) => response.json())
    .then((data) => data);
  return fetchData;
}

function getPokemonForms(url) {
  let fetchData = fetch(url)
    .then((response) => response.json())
    .then((data) => data);
  return fetchData;
}

async function fetchPokemon(id) {
  const pokemonInfo = await getPokemon(id);
  const pokemonForms = await getPokemonForms(pokemonInfo.forms[0].url)
  pokemonCard(pokemonInfo,pokemonForms)
}

function pokemonCard({name, height, base_experience},{is_battle_only,sprites}){
  const card = `
  <ul>
  <li> nombre: ${name} </li>
  <li> height: ${height} </li>
  <li> Base exp: ${base_experience} </li>
  <li> BattleOnly? ${is_battle_only} </li>
  <li> <img src="${sprites.front_default}"> </li>
  </ul>
  `;
  showInBrowser(card)
}

function showInBrowser(content){
  $('.card').html(content)
}

fetchPokemon(1);

/////////////////////////////////////////
function getPokemonInfo(pkId) {
  let fetchData = fetch(`https://pokeapi.co/api/v2/pokemon/${pkId}`)
    .then((response) => response.json())
    .then((data) => data);
  return fetchData;
}

function getPokemonInfoForms(url) {
  let fetchData = fetch(url)
    .then((response) => response.json())
    .then((data) => data);
  return fetchData;
}

async function searchPokemon(){
  $('.modal').modal('show');
  const pokemonInfo = await getPokemonInfo($('#search').val());
  const pokemonForms = await getPokemonInfoForms(pokemonInfo.forms[0].url)
  $('.modal').modal('hide');
  showCard(pokemonInfo, pokemonForms);
}

function showCard({name, height, base_experience, moves},{sprites}){
  let mv = '';
  let mov = moves.map(move => mv ? mv += ', ' + move['move']['name'] : mv = move['move']['name']);
  const card = `
  <div class="card">
    <h5 class="card-title">${name}</h5>
    <div class="card-body">
      <img src="${sprites.front_default}">
      <p class="card-text">
        <ul>
        <li> height: ${height} </li>
        <li> Base exp: ${base_experience} </li>
        <li> Moves: ${mov} </li>
        </ul>
      </p>
    </div>
  </div>
  `;
  $('.poke-card').html(card);
}

